//Declaração 
var variavel;
variavel = 2;
variavel = 'blabla';

console.log('Conteudo da variavel', variavel);

// Condição

if(variavel === 'blabla') {
    console.log('é verdadeiro')
}else{
    console.log('é falso')
}

// Ternário
var conteudo = (true) ? 'Ternário Verdadeiro' : 'Ternário Falso';
console.log('Valor da variável', conteudo);

// Repetição
console.log('Repetição');
for (let i = 0; i < 10; i++) {
    console.log(i);
}

// Estrutura
var obj;
obj = {
    nome: 'Vitor',
    idade: 21
}
console.log(obj);