let i: string = "False";
console.log("Teste ==>", i);

class Item {
  nome: string;
  descricao: string;
  constructor(nome: string, desc: string) {
    this.nome = nome;
    this.descricao = desc;
  }
}

class Mochila {
  itens: Array<Item> = [];
  capacidadeMaxima: number;
  constructor(capacidade: number) {
    this.capacidadeMaxima = capacidade;
  }
}

class Personagem {
  nickname: string;
  level: number;
  forca: number;
  agilidade: number;
  stamina: number;
  mochila?: Mochila;

  constructor(
    nick: string,
    level: number,
    forca: number,
    agilidade: number,
    stamina: number
  ) {
    this.nickname = nick;
    this.forca = forca;
    this.stamina = stamina;
    this.agilidade = agilidade;
    this.level = level;
  }

  aumentarLevel() {
    this.agilidade++;
    this.stamina++;
    this.forca++;
  }
}

let norton: Personagem = new Personagem("Girafossauro", 98, 2, 6, 0);
norton.mochila = new Mochila(3);
norton.mochila.itens.push(new Item("Estrela da Manha", "Arma medieval"));

console.log("Personagem Norton ==>", norton);

norton.aumentarLevel();
console.log("UUPP ==>", norton);

//Atividade Criação de Classes
class AngularApp {
  developmentVersion: string;
  pageTitle: string;
  qtdComponents: number;
  integratedAPI: boolean;
  uiLibrary: string;

  constructor(
    devVersion: string,
    title: string,
    qtdComponents: number,
    integratedAPI: boolean,
    uiLibrary: string
  ) {
    this.developmentVersion = devVersion;
    this.pageTitle = title;
    this.qtdComponents = qtdComponents;
    this.integratedAPI = integratedAPI;
    this.uiLibrary = uiLibrary;
  }

  get version(): string {
    return this.developmentVersion;
  }

  set title(val: string) {
    this.pageTitle = val;
  }
}

let myPage: AngularApp = new AngularApp(
  "9",
  "Projeto de Programas",
  9,
  true,
  "Nebular"
);

let taskManager: AngularApp = new AngularApp(
  "11.2.3",
  "Task Manager",
  6,
  true,
  "Tailwind"
);
let capibaraTestBootCamp: AngularApp = new AngularApp(
  "10",
  "capybara",
  4,
  false,
  "Bootstrap"
);

console.log("Minha Pagina", myPage);

myPage.title = "Meteoro";

console.log("Minha Pagina", myPage, taskManager, capibaraTestBootCamp);

class FoneDeOuvido {
  isBluetooth: boolean;
  mark: string;
  connector: string;
  batteryLife: number;

  constructor(
    isBluetooth: boolean,
    mark: string,
    connector: string,
    batteryLife: number
  ) {
    this.isBluetooth = isBluetooth;
    this.mark = mark;
    this.connector = connector;
    this.batteryLife = batteryLife;
  }
}

let miBand: FoneDeOuvido = new FoneDeOuvido(true, "Xiaomi", "USB Mini", 6);
let tws: FoneDeOuvido = new FoneDeOuvido(true, "Blitzwolf", "Micro USB", 12);
let hyperX: FoneDeOuvido = new FoneDeOuvido(true, "Kingston", "USB C", 4);

console.log("Fones de Ouvido ==>", miBand, tws, hyperX);

class Livro {
  author: string;
  title: string;
  numPages: number;
  hardCover: boolean;
  edition: string;
  yearOfPublication: number;
  category: string;

  constructor(
    author: string,
    title: string,
    numPages: number,
    hardCover: boolean,
    edition: string,
    yearOfPublication: number,
    category: string
  ) {
    this.author = author;
    this.title = title;
    this.numPages = numPages;
    this.hardCover = hardCover;
    this.edition = edition;
    this.yearOfPublication = yearOfPublication;
    this.category = category;
  }
}

let ordemDaFenix: Livro = new Livro(
  "J. K. Rowling",
  "Harry Potter e a Ordem da Fenix",
  766,
  true,
  "Edição Ilustrada",
  2003,
  "Fantasia"
);

let geometriaAnalitica: Livro = new Livro(
  "Paulo Winterle",
  "Geometria Analítica",
  304,
  false,
  "Primeira",
  1995,
  "Didatico"
);

let coraline: Livro = new Livro(
  "Neil Gaiman",
  "Coraline",
  148,
  true,
  "Primeira",
  2002,
  "Literatura fantastica"
);
