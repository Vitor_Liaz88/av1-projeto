# AV1 - Projeto
## Descrição do Projeto
<p align ="center">Estruturas de repetição e condicional e criação de classes</p>

<h4 align="center"> Projeto em desenvolvimento ⚠️ </h4>

### Pré-requisitos

Primeiramente para utilizar a aplicação você precisa configurar seu ambiente \n de desenvolvimento instalando as ferramentas: 
[Node.js](https://nodejs.org/en/)
Editor de texto recomendado [VSCode](https://code.visualstudio.com/)

```bash

# Clone o repositório
$ git clone <https://gitlab.com/Vitor_Liaz88/av1-projeto.git>

# Instale as depêndencias
$ npm install

# Instale o typescript em modo de desenvolvimento
$ npm install typescript -d

# Execute a aplicação
$ npm start

```
### Tecnologias

- [Node.js](https://nodejs.org/en/)
- [TypeScript](https://www.typescriptlang.org/)

